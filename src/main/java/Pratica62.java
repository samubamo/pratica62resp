
import java.util.Collections;
import java.util.List;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica62 {
    public static void main(String[] args) {
        Time time1 = new Time();
        
        JogadorComparator jc = new JogadorComparator(true,true, false);
        
        time1.addJogador(new Jogador(1, "Fernanda Garay"));
        time1.addJogador(new Jogador(2, "Tandara"));
        time1.addJogador(new Jogador(33, "Sheilla"));
        
        time1.ordena(jc);
        
        for(Jogador time: time1.getJogadores()){
            System.out.println(time);
        }
        
        int posicao = Collections.binarySearch(time1.getJogadores(),new Jogador(1, "Fernanda Garay"));
        
        System.out.println("Posição no time: " + posicao);
        System.out.println("Nome: " + time1.getJogadores().get(posicao).getNome());
        System.out.println("Numero: " + time1.getJogadores().get(posicao).getNumero());
    }
}
