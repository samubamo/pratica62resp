/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

/**
 *
 * @author samu
 */
public class JogadorComparator implements Comparator<Jogador> {
    private boolean ordNum;
    private boolean ordNome;
    private boolean asc;

    public JogadorComparator() {
    }
     
     public JogadorComparator(boolean ordNum, boolean asc, boolean ordNome){
         this.ordNum = ordNum;
         this.asc = asc;
         this.ordNome = ordNome;
    }
     
    @Override
     public int compare(Jogador j1, Jogador j2){
         if(ordNum && asc){
             return j1.compareTo(j2);
         }
         if(ordNum && !asc){
             return -1*(j1.compareTo(j2));
         }
         if(ordNome && !asc){
             return -1*(j1.getNome().compareTo(j2.getNome()));
         }
             return j1.getNome().compareTo(j2.getNome());
    }
}